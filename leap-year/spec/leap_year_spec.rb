require_relative File.join(__FILE__, '..', '..', 'leap_year.rb')

describe '#leap1' do
  context 'When the given year is divisible with 4' do
    it 'it returns true' do
      expect(leap1?(2016)).to be_truthy
    end
  end

  context 'When the given year does not divisible with 4' do
    it 'it returns false' do
      expect(leap1?(2015)).to be_falsey
    end
  end

  context 'When the given year is divisible with 100' do
    it 'it returns false' do
      expect(leap1?(1900)).to be_falsey
    end
  end

  context 'When the given year is divisible with 400' do
    it 'it returns true' do
      expect(leap1?(2000)).to be_truthy
    end
  end
end

describe '#leap2?' do
  context 'When the given year is divisible with 4' do
    it 'it returns true' do
      expect(leap2?(2016)).to be_truthy
    end
  end

  context 'When the given year does not divisible with 4' do
    it 'it returns false' do
      expect(leap2?(2015)).to be_falsey
    end
  end

  context 'When the given year is divisible with 100' do
    it 'it returns false' do
      expect(leap2?(1900)).to be_falsey
    end
  end

  context 'When the given year is divisible with 400' do
    it 'it returns true' do
      expect(leap2?(2000)).to be_truthy
    end
  end
end
