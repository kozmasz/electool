require 'date'

def leap1? year
  Date.new(year, 1, 1).leap?
end

def leap2? year
  (year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0)
end
