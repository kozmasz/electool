# it works but factorial(0) is nil
#def factorial(n)
#  (1..n).reduce(:*)
#end

def factorial(n)
  if n == 0
    1
  else
    n *= factorial(n-1)
  end
end

# binomial
# (n)
# (k)
def binomial(n,k)
  return 1 if n-k <= 0
  return 1 if k <= 0
  factorial(n) / (factorial(k) * factorial(n - k))
end

def pascals_triangle(row_number)
  (0..row_number).inject([]) do |ret, current_row|
    ret.push((0..current_row).map {|number| binomial(current_row, number) })
    ret
  end
end
