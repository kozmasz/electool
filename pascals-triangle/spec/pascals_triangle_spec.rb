require_relative File.join(__FILE__, '..', '..', 'pascals_triangle.rb')

describe '#factorial' do
  context 'When the given number is 0' do
    it 'it returns 1' do
      expect(factorial(0)).to eq(1)
    end
  end

  context 'When the given number is 5' do
    it 'it returns 120' do
      expect(factorial(5)).to eq(120)
    end
  end
end

describe '#binomial' do
  context 'When n is 90 and k is 5' do
    it 'it returns with the variations of 5 lottery(43949268)' do
      expect(binomial(90, 5)).to eq(43949268)
    end
  end
end

describe '#pascals_triangle' do
  context 'When the given parameter is 5' do
    let(:array) { [[1],
                   [1, 1],
                   [1, 2, 1],
                   [1, 3, 3, 1],
                   [1, 4, 6, 4, 1],
                   [1, 5, 10, 10, 5, 1]] }
    it 'it returns with 5 row long pascal triangle' do
      expect(pascals_triangle(5)).to match_array(array)
    end
  end
end
