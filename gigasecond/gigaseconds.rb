require 'date'

def gigasecond(birthdate)
  (Date.parse(birthdate).to_time + 1_000_000_000).strftime('%Y-%m-%d')
end
