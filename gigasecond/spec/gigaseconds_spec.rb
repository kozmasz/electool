require_relative File.join(__FILE__, '..', '..', 'gigaseconds.rb')

describe '#gigaseconds' do
  it 'it returns the date of celebrate the 1 Gs anniversary.' do
    expect(gigasecond('1989-12-30')).to eq("2021-09-07")
  end
end
